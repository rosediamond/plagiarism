from flask import Flask, request
from flask_restful import Api, Resource
from pymongo import MongoClient
import bcrypt
import spacy


app = Flask(__name__)
api = Api(app)

client = MongoClient("mongodb://db:27017")
db = client.SimilarDB
user = db["Users"]


def UserExist(username):
    if user.find({"Username": username}).count():
        return True
    else:
        return False


def countToken(username):
    if UserExist(username):
        token = user.find({"Username": username})[0]["Tokens"]
        return token 


def verify_Pw(username, password):
    if UserExist(username):
        pwd = user.find({"Username": username})[0]["Password"]
        if bcrypt.hashpw(password.encode('utf8'), pwd) == pwd:
            return True
        else:
            return False
    else:
        return False


class Register(Resource):
    def post(self):
        postedData = request.get_json()
        username = postedData["username"]
        password = postedData["password"]
        if UserExist(username):
            return {"Status": 302,
                    "msg": "This User already register"}, 302

        hashed_pw = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())
        user.insert({"Username": username,
                     "Password": hashed_pw,
                     "Tokens": 6})
        return {"status": 200,
                "msg": "you have sucessful register"}, 200


class Detect(Resource):
    def post(self):
        postedData = request.get_json()
        username = postedData["username"]
        password = postedData["password"]
        text1 = postedData["text1"]
        text2 = postedData["text2"]
        verifyuser = verify_Pw(username, password)
        if not verifyuser:
            return {"status": 301,
                    "msg": "Invalid Username or Password"}, 301

        num_token = countToken(username)
        if num_token <= 0:
            return {"status": 303,
                    "msg": "out of token"}, 303

        nlp = spacy.load('en_core_web_sm')
        text1 = nlp(text1)
        text2 = nlp(text2)

        ratio = text1.similarity(text2)
        user.update({"Username": username}, {"$set": {"Tokens": num_token-1}})
        return {"status": 200,
                "similarity": ratio,
                "Tokens": user.find({"Username": username})[0]["Tokens"]}, 200

class Refill(Resource):
    def post(self):
        postedData = request.get_json()
        username = postedData["username"]
        admin_pw = postedData["password"]
        refill_amount= postedData["refill"]   
        adminpw="123xy"
        if not UserExist(username):
            return {"status":301,"msg":"Invalid user name"},301
        if  admin_pw != adminpw:
            return {"status":301,"msg":"Invalid admin password you cannot refill"},301
          
        user.update({"Username":username},{"$set":{"Tokens":refill_amount}})
        return {"status":200,"msg":"suceesfull refill",
        "refilamount":refill_amount},200
   

api.add_resource(Register, "/register")
api.add_resource(Detect, "/detect")
api.add_resource(Refill, "/refill")

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
